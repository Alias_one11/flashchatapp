import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBAction func registerPressed(_ sender: UIButton) {
        // ||__________||
        guard let email = emailTextfield.text,
            !email.isEmpty,
            let pwd = passwordTextfield.text,
            !pwd.isEmpty else { return }
        
        Auth.auth().createUser(withEmail: email, password: pwd) { (_, error) in
            // MARK: - FirebaseAuth
            if let error = error {
                return printf("Could not create user..\n\(error.localizedDescription)")
            } else {
                // If no error is found it will create an account in
                // firebaseAuth & segue into the next designated controller
                self.performSegue(withIdentifier: "RegisterToChat", sender: self)
            }
            
            // MARK: - Firestore
            
        }
    }
    
}
