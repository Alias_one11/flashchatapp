import UIKit

class WelcomeViewController: UIViewController {
    // MARK: - @IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        typingAnimation()
    }
    
    
    // MARK: - Helper methods
    func typingAnimation() {
        // ||__________||
        titleLabel.text = ""
        var charIndex = 0.0
        let titleTxt = "⚡️FlashChat"
        
        for char in titleTxt {
            // Add a timer to animate title
            Timer.scheduledTimer(withTimeInterval: 0.1 * charIndex, repeats: false) { (timer) in
                self.titleLabel.text?.append(char)
            };charIndex += 1
        }
    }
}// END OF CLASS
