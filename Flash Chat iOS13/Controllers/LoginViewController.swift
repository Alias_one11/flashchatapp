import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBAction func loginPressed(_ sender: UIButton) {
        // ||__________||
        guard let email = emailTextfield.text,
            !email.isEmpty,
            let pwd = passwordTextfield.text,
            !pwd.isEmpty else { return }
        
        Auth.auth().signIn(withEmail: email, password: pwd) { /* [weak self] */ result, error in
            /** Prevents a retain cycle:
             A memory leak in iOS is when an amount of allocated space in memory cannot be deallocated due to retain cycles. Since Swift uses Automatic Reference Counting (ARC), a retain cycle occurs when two or more objects holds strong references to each other. As a result these objects retain each other in memory because their retain count would never decrement to 0, which would prevent deinit from ever being called and memory from being freed. */
            // guard let self = self else { return }
            
            if let error = error {
                return printf("Could not login..\n\(error.localizedDescription)")
            } else {
                self.performSegue(withIdentifier: "LoginToChat", sender: self)
            }
        }
    }
}
